#!/usr/bin/env python
# -*- coding:utf-8 -*-



import os
import sys
import django
import inspect
from datetime import timedelta

from pathlib import Path
from types import ModuleType
from django.conf import settings
from django.urls import re_path, include, path
from django.http import HttpResponse
from django.contrib import admin
from django.views.static import serve
from django.apps import apps  # noqa: E402 isort:skip

from django.db import models
from django.contrib import admin

BASE_DIR = Path(__file__).resolve().parent
# The current name of the file, which will be the name of our app
APP_LABEL = Path(__file__).stem
# Migrations folder need to be created, and django needs to be told where it is
APP_MIGRATION_MODULE = '%s_migrations' % APP_LABEL
APP_MIGRATION_PATH = BASE_DIR / APP_MIGRATION_MODULE

# Create the folder and a __init__.py if they don't exist
if not APP_MIGRATION_PATH.exists():
    APP_MIGRATION_PATH.mkdir()
    (APP_MIGRATION_PATH / '__init__.py').touch()

# Hack to trick Django into thinking this file is actually a package
sys.modules[APP_LABEL] = sys.modules[__name__]
sys.modules[APP_LABEL].__path__ = [Path(__file__).resolve()]

settings.configure(
    DEBUG=True,
    SECRET_KEY=os.environ.get('SECRET_KEY', os.urandom(32)),
    ALLOWED_HOSTS=os.environ.get('ALLOWED_HOSTS', '*').split(','),
    ROOT_URLCONF=f'{APP_LABEL}.urls',
    MIDDLEWARE=[
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'corsheaders.middleware.CorsMiddleware',
    ],
    INSTALLED_APPS=[
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        APP_LABEL,
        "corsheaders",
        "rest_framework",
        "drf_spectacular",
        "rest_framework_simplejwt",
    ],
    MIGRATION_MODULES={APP_LABEL: APP_MIGRATION_MODULE},
    SITE_ID=1,
    STATIC_URL="static/",
    STATIC_ROOT=BASE_DIR/'static',
    MEDIA_URL='/media/',
    TEMPLATES=[
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [BASE_DIR/"templates",],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.i18n',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.template.context_processors.tz',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ],
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    },
    REST_FRAMEWORK={
        # YOUR SETTINGS
        'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
        'DEFAULT_AUTHENTICATION_CLASSES': [
            'rest_framework.authentication.BasicAuthentication',
            'rest_framework.authentication.SessionAuthentication',
            'rest_framework_simplejwt.authentication.JWTAuthentication',
        ],
        # 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.CursorPagination',
        # 'PAGE_SIZE': 100,
        'DEFAULT_PERMISSION_CLASSES': [
            'rest_framework.permissions.IsAuthenticated',
        ]
    },
    CORS_ORIGIN_ALLOW_ALL = True,
    SPECTACULAR_SETTINGS = {
        'TITLE': f'Your {APP_LABEL} API',
        'DESCRIPTION': f'Your {APP_LABEL} Rest API description',
        'VERSION': '1.0.0',
        'SERVE_INCLUDE_SCHEMA': False,
    },
    SIMPLE_JWT = {
        'ACCESS_TOKEN_LIFETIME': timedelta(minutes=30),
        'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    },
    LANGUAGE_CODE = 'en-us',
    TIME_ZONE = 'UTC',
    USE_I18N = True,
    USE_TZ = True,
    # Celery Configuration Options
    # https://docs.celeryq.dev/en/v5.3.6/userguide/configuration.html#configuration
    CELERY_TASK_TRACK_STARTED = True,
    CELERY_TASK_TIME_LIMIT = 30 * 60,
    CELERY_BROKER_URL = 'redis://127.0.0.1:6379/0',
    BROKER_URL = 'redis://127.0.0.1:6379/0',
    CELERY_RESULT_BACKEND = 'redis://localhost:6379',
    CELERY_ACCEPT_CONTENT = ['application/json'],
    CELERY_TASK_SERIALIZER = 'json',
    CELERY_RESULT_SERIALIZER = 'json',
    CELERY_TIMEZONE = 'UTC',
    DEFAULT_AUTO_FIELD="django.db.models.BigAutoField",
)

django.setup()

# Setup the AppConfig so we don't have to add the app_label to all our models
def get_containing_app_config(module):
    if module == '__main__':
        return apps.get_app_config(APP_LABEL)
    return apps._get_containing_app_config(module)


apps._get_containing_app_config = apps.get_containing_app_config
apps.get_containing_app_config = get_containing_app_config


# Your code below this line
# ##############################################################################

# Create your models between here...

class Foo(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        ordering = ["id"]

# And here

for model in apps.get_models():
    try:
        admin.site.register(model)
    except:
        pass
admin.autodiscover()

from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import routers
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView


# Create your serializers and views between here...

class FooSerializer(serializers.ModelSerializer):
    class Meta:
        model = Foo
        fields = '__all__'

class FooViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Foo.objects.all()
    serializer_class = FooSerializer


router = routers.DefaultRouter()
router.register(r'foo', FooViewSet)
# And here


# Unless you want to tweak your urls, your code should end above this line
# ########################################################################

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^api/', include(router.urls)),
    re_path(r'^api-auth/', include('rest_framework.urls',\
                                   namespace='rest_framework')),

    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('schema/swagger-ui/',
         SpectacularSwaggerView.as_view(url_name='schema'),
         name='swagger-ui'
    ),
    # JWT
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    # serve static files
    re_path(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    re_path(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
]

# Used so you can do 'from <name of file>.models import *'
models_module = ModuleType(f'{APP_LABEL}.models')
urls_module = ModuleType(f'{APP_LABEL}.urls')

urls_module.urlpatterns = urlpatterns
for variable_name, value in list(locals().items()):
    # We are only interested in models
    if inspect.isclass(value) and issubclass(value, models.Model):
        setattr(models_module, variable_name, value)

# Setup the fake modules
sys.modules[models_module.__name__] = models_module
sys.modules[urls_module.__name__] = urls_module
sys.modules[APP_LABEL].models = models_module
sys.modules[APP_LABEL].urls = urls_module


if __name__ == "__main__":
    from django.core.management import execute_from_command_line
    from django.contrib.auth import get_user_model
    from django.core.management.base import BaseCommand
    class Command(BaseCommand):
        help = "Creates an admin user non-interactively if it doesn't exist"
        def handle(self, *args, **options):
            User = get_user_model()
            if not User.objects.filter(username="nsukami").exists():
                User.objects.create_superuser(
                    username="nsukami",
                    email="ptrck@nskm.xyz",
                    password="1234"
                )
            self.stdout.write(
                self.style.SUCCESS(f'Super user nsukami created successfully')
            )

    execute_from_command_line(sys.argv)
else:
    from django.core.wsgi import get_wsgi_application
    get_wsgi_application()
